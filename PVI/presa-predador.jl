using Plots
println("Todo dia eu acordo brasileiro")

function f(t, y)
    fv = zeros(Float64,2)
    fv[1] = 1.2*y[1] - 0.6*y[1]*y[2]
    fv[2] = -0.8*y[2] + 0.3*y[1]*y[2]
    return fv
end

function euler(_a, _w0, _h, _N)
    w = zeros(Float64,2,_N)
    w[1,1] = _w0[1]
    w[2,1] = _w0[2]
    for i=1:_N-1
        ti=_a+(i-1)*_h
        w[:,i+1] = w[:,i] + _h * f(ti,w[:,i])
    end
    return w
end

function rk(_a, _w0, _h, _N)
    w = zeros(Float64,2,_N)
    w[1,1] = _w0[1]
    w[2,1] = _w0[2]
    for i=1:_N-1
        ti = _a+(i-1)*_h
        k1 = f(ti,w[:,i])
        k2 = f(ti+_h/2,w[:,i]+_h*k1/2)
        k3 = f(ti+_h/2,w[:,i]+_h*k2/2)
        k4 = f(ti+_h,w[:,i]+_h*k3)
        phi = k1/6+k2/3+k3/3+k4/6
        w[:,i+1] = w[:,i] + _h * phi
    end
    return w
end

function main()
    a = 0.0
    b = 4.0
    h = 0.02
    w0 = zeros(Float64,2)
    w0[1] = 2
    w0[2] = 1
    N= Int64((b-a)/h)
    we = euler(a,w0,h,N)
    wr = rk(a,w0,h,N)

    t = zeros(Float64, N)
    t[1] = a
    for i=1:N-1
        t[i+1] = a+i*h
    end

    @show we
    @show wr
    @show N
    plot(t,we[1,:])
    plot!(t,we[2,:])
    plot!(t,wr[1,:])
    plot!(t,wr[2,:])
end

main()